package vitaliy.dictionary.ui;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import vitaliy.dictionary.R;
import vitaliy.dictionary.db.DBHelper;
import vitaliy.dictionary.db.DataBaseProvider;
import vitaliy.dictionary.db.URI_BASE;
import vitaliy.dictionary.rest.Matche;
import vitaliy.dictionary.rest.Rest;
import vitaliy.dictionary.rest.RestCalback;
import vitaliy.dictionary.rest.Translation;
import vitaliy.dictionary.tools.CheckTools;
import vitaliy.dictionary.tools.DialogUniversal;
import vitaliy.dictionary.tools.I_Action;

/**
 * Created by Vitaliy on 24.01.2015.
 */
public class FragmentMain extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {
    ListView listView;
    SimpleCursorAdapter adapter;
    EditText editText;
    Loader<Cursor> cursorLoader;
    private int LOADER_ID = 1;


    public static FragmentMain newInstance() {
        FragmentMain f = new FragmentMain();
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_main, container, false);
        listView = (ListView) v.findViewById(R.id.listView);
        editText = (EditText) v.findViewById(R.id.editText);
        editText.setOnEditorActionListener(getEditorAction());
         editText.addTextChangedListener(new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence s, int start, int count, int after) {
           }

           @Override
           public void onTextChanged(CharSequence s, int start, int before, int count) {
               Log.d("test","on Text changet cursorLoader"+cursorLoader);
                    if(cursorLoader!=null) {
                        ((CursorLoader)cursorLoader).setSelectionArgs(new String[]{"%" + s + "%"});
                        cursorLoader.forceLoad();
                    }
           }
           @Override
           public void afterTextChanged(Editable s) {

           }
       });
        listView.setOnItemClickListener(getDictionaryAdupter());
        return v;
    }

    private AdapterView.OnItemClickListener getDictionaryAdupter() {
        return new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
              Cursor cursor=  adapter.getCursor();
                cursor   .moveToPosition(position);

                createDiscriptionDilog(cursor.getString(cursor.getColumnIndex(DBHelper.DICTIONARY_WORD)),
                        cursor.getString(cursor.getColumnIndex(DBHelper.DICTIONARY_TRANSLATION) ) ).show();
            }
        };
    }

    private Dialog createDiscriptionDilog(String origin,String translate) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        LinearLayout view = (LinearLayout) inflater.inflate(R.layout.dialog_word_translate, null);
        builder.setView(view);
        ((TextView)view.findViewById(R.id.word_origin)).setText(origin);
        ((TextView)view.findViewById(R.id.word_translate)).setText(translate);
        builder.setPositiveButton("ОК", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return  builder.create();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        String from[] = {DBHelper.DICTIONARY_WORD, DBHelper.DICTIONARY_TRANSLATION};
        int to[] = {android.R.id.text1, android.R.id.text2};
        adapter = new SimpleCursorAdapter(getActivity(), android.R.layout.simple_list_item_1, null, from, to);
        listView.setAdapter(adapter);
        getLoaderManager().initLoader(LOADER_ID, null, this);
        if(savedInstanceState!=null)
        {
            cursorLoader=getLoaderManager().getLoader(LOADER_ID);
        }

    }

    private TextView.OnEditorActionListener getEditorAction() {
        return new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                switch (actionId) {
                    case EditorInfo.IME_ACTION_SEARCH:

                      Rest r=  new Rest(new RestCalback() {
                            @Override
                            public void start() {
                                startProgresBar();
                            }

                            @Override
                            public void complite(Translation translation) {
                                stopProgresbar();

                               if( translation.isSucsesTranslation())
                                   DialogUniversal.createDialogList(getActivity(), getResources().getString(R.string.dialog_translation_title),
                                          getResources().getString(R.string.dialog_translation_string),
                                          translation,
                                         getTranslationAceptAction());
                                else DialogUniversal.createDialogMassage(getActivity(),getResources().getString(R.string.dialog_translation_error)).show();
                            }
                        });
                        String toTranslate=editText.getText().toString();
                        if(isUnical(toTranslate)) {
                            if (!TextUtils.isEmpty(toTranslate))
                                if (CheckTools.isOnline(getActivity()))
                                    try {
                                        r.translate(toTranslate);
                                    } catch (Exception e) {
                                        DialogUniversal.createDialogMassage(getActivity(), getResources().getString(R.string.language_is_not_defined));
                                    }
                                else
                                    DialogUniversal.createDialogMassage(getActivity(), getResources().getString(R.string.no_internet_connection));
                        }else  DialogUniversal.createDialogMassage(getActivity(), getResources().getString(R.string.alredy_translate));
                        break;
                }
                return false;
            }


        };
    }

    private boolean isUnical(String toTranslate) {
        DBHelper dbHelper=new DBHelper(getActivity());
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor cursor = db.query(DBHelper.TABL_DICTIONARY, new String[]{DBHelper.DICTIONARY_WORD}, DBHelper.DICTIONARY_WORD+"= ?",
                new String[]{toTranslate}, null, null, null);

        return !cursor.moveToFirst();
    }

    private void startProgresBar() {
        ((ActionBarActivity)getActivity()).setSupportProgressBarIndeterminateVisibility(true);
    }

    private I_Action getTranslationAceptAction() {
       return new I_Action() {


           @Override
           public void action(Matche matche) {
               addToBd(matche);
           }
       } ;
    }

    private void stopProgresbar() {
        ((ActionBarActivity)getActivity()).setSupportProgressBarIndeterminateVisibility(false);
    }

    private void addToBd(Matche matche) {
        Uri CONTACT_URI = Uri.parse(URI_BASE.CONTENT_URI + URI_BASE.DICTIONARY);
        ContentValues cv = new ContentValues();
        cv.put(DBHelper.DICTIONARY_WORD, matche.getSegment());
        cv.put(DBHelper.DICTIONARY_WORD_forLike, matche.getSegment().toLowerCase());
        cv.put(DBHelper.DICTIONARY_TRANSLATION, matche.getTranslation());
        getActivity().getContentResolver().insert(CONTACT_URI, cv);
        getLoaderManager().getLoader(LOADER_ID).forceLoad();
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri CONTACT_URI = Uri.parse(URI_BASE.CONTENT_URI + URI_BASE.DICTIONARY);
        String[] Like=null;
        if(editText!=null)
            Like   =new String[]{"%"+editText.getText().toString()+"%"};
        cursorLoader=new  CursorLoader(getActivity(), CONTACT_URI, null, null, Like, null);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adapter.swapCursor(data);
    }



    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

}
