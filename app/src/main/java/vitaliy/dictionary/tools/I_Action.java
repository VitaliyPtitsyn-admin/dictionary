package vitaliy.dictionary.tools;

import vitaliy.dictionary.rest.Matche;
import vitaliy.dictionary.rest.Translation;

/**
 * Created by Vitaliy on 25.01.2015.
 */
public interface I_Action {
    void action(Matche matche);
}
