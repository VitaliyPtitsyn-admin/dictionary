package vitaliy.dictionary.tools;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.List;

import vitaliy.dictionary.R;
import vitaliy.dictionary.rest.Matche;
import vitaliy.dictionary.rest.Translation;


/**
 * Created by HP on 20.10.2014.
 */
public class DialogUniversal {

    public static void createDialogUniversal(Context context, String title, String actionString, Translation translation, I_Action acept) {
        if (translation.isMatchVariants()) {
            createDialogSingle(context, title, actionString, translation.getTranslationsObject().get(0), acept);
            return;
        } else {
            createDialogList(context, title, actionString, translation, acept);
            return;
        }

    }


    public static Dialog createDialogList(Context context, String title, String actionString, final Translation translation, final I_Action acep) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder.setTitle(title);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        FrameLayout view = (FrameLayout) inflater.inflate(R.layout.dialog_list, null);
        builder.setView(view);
        TextView text = (TextView) view.findViewById(R.id.dialog_text_context);
        text.setText(actionString);
        ListView listContent = (ListView) view.findViewById(R.id.dialog_listview);

        final AlertDialog alert = builder.create();
        listContent.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                acep.action(translation.getTranslationsObject().get(position));
                alert.dismiss();
            }
        });
        ArrayAdapter adupter = new ArrayAdapter(context, android.R.layout.simple_list_item_1, translation.getTranslations());
        listContent.setAdapter(adupter);
        builder.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alert.show();
        return alert;
    }

    private static Dialog createDialogSingle(Context context, String title, String actionString, final Matche matche, final I_Action acept) {
        final AlertDialog.Builder adb = new AlertDialog.Builder(context);
        adb.setTitle(title);
        adb.setMessage(actionString + "\n" + title);
        adb.setPositiveButton("Да", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (acept != null)
                    acept.action(matche);
            }
        });
        adb.setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return adb.show();
    }
    public static Dialog createDialogMassage(Context context,String string) {
        final AlertDialog.Builder adb = new AlertDialog.Builder(context);
        adb.setTitle(string);
        adb.setNegativeButton("ок", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        return adb.show();
    }
}



