package vitaliy.dictionary.rest;

import android.database.Cursor;
import android.os.AsyncTask;
import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;

import retrofit.RestAdapter;
import vitaliy.dictionary.tools.CheckTools;

/**
 * Created by Vitaliy on 24.01.2015.
 */
public class Rest extends AsyncTask<String,Void,Translation>
{
	RestCalback calback;
    String languageTranslation;
	private static final String API_URL="http://api.mymemory.translated.net";

	public Rest(RestCalback calback) {
		this.calback = calback;
	}

	public void translate(String word) throws Exception {


       boolean languageDetec=false;
        for(int i=0;i<word.length();i++)
        {
            languageDetec= languageDetect(word.charAt(i));
            if(languageDetec)
                break;
        }
        if(!languageDetec)
            throw  new Exception();
        execute(word);
	}

    private boolean languageDetect(char c) {
        if(c>=97  & c <=122) {
            languageTranslation="en|ru";
            return true;
        } if(c>=129  & c <=154) {
            languageTranslation = "en|ru";
            return true;
        }
        if(c>='a'  & c <='я') {
            languageTranslation = "ru|en";
            return true;
        }
        if(c>='А'  & c <='Я') {
            languageTranslation = "ru|en";
            return true;
        }

            return false;
    }


    @Override
	protected void onPreExecute() {
		super.onPreExecute();
		calback.start();
	}

	@Override
	protected Translation doInBackground(String... params) {
		// Create a very simple REST adapter which points the GitHub API endpoint.
		RestAdapter restAdapter = new RestAdapter.Builder()
				.setEndpoint(API_URL)
				.build();

		// Create an instance of our GitHub API interface.
		Translate translate = restAdapter.create(Translate.class);

		// Fetch and print a list of the contributors to this library.
		Translation contributors = null;
			contributors = translate.translate( params[0],languageTranslation );
			Log.d("translate", contributors.responderId + " responseStatus=" + contributors.responseStatus+" size "+contributors.matches.size());
		for (Matche matche : contributors.matches)
			Log.d("translate","segment " +matche.segment+" translation  "+matche.translation);
//		}
		return contributors;
	}



	@Override
	protected void onPostExecute(Translation translation) {
		super.onPostExecute(translation);
		calback.complite(translation);
	}

}
