package vitaliy.dictionary.rest;

import java.util.List;

import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Vitaliy on 24.01.2015.
 */
public class Translation
{
    private static final String SUCSES ="200" ;
    String responderId;
	String responseStatus;
	List<Matche> matches;

    public boolean isSucsesTranslation() {
        return responseStatus.equals(SUCSES);
    }

    public String[] getTranslations() {
        String[] Return= new String[matches.size()];
        for(int i=0;i<matches.size();i++)
            Return[i]=matches.get(i).translation;
        return Return;
    }

    public boolean isMatchVariants() {
        return false;
    }

    public List<Matche> getTranslationsObject() {
        return  matches;
    }
}

interface Translate {
	@GET("/get?&")
	Translation translate(
			@Query("q") String q,
			@Query("langpair") String langpair	);
}