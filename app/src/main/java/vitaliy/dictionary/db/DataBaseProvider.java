package vitaliy.dictionary.db;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.text.TextUtils;

/**
 * Created by Vitaliy on 24.01.2015.
 */
public class DataBaseProvider extends ContentProvider implements URI_BASE {

	DBHelper dbHelper;
	SQLiteDatabase db;

	private static final UriMatcher uriMatcher;

	static {
		uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		uriMatcher.addURI(AUTHORITY, DICTIONARY, URI_DICTIONARY);
		uriMatcher.addURI(AUTHORITY, WORD, URI_WORD);
		uriMatcher.addURI(AUTHORITY, WORD_TRANSLATION, URI_WORD_TRANSLATION);
	}

	@Override
	public boolean onCreate() {

		dbHelper = new DBHelper(getContext());
		return true;
	}


	public Cursor query(Uri uri, String[] columns, String selection, String[] selectionArgs, String sortOrder) {

        selection=  checkSelection(selectionArgs,selection);
		switch (uriMatcher.match(uri)) {
			case URI_WORD:
				columns=	checkColum(columns,DBHelper.DICTIONARY_WORD);

				break;
			case URI_DICTIONARY:
				columns=	checkColum(columns,DBHelper.DICTIONARY_WORD);
				columns=	checkColum(columns,DBHelper.DICTIONARY_TRANSLATION);
				columns=	checkColum(columns,DBHelper.DICTIONARY_ID);
                if(sortOrder==null)
                    sortOrder=dbHelper.DICTIONARY_ID+" DESC";
				break;
			case URI_WORD_TRANSLATION:
				String id = uri.getLastPathSegment();
				columns=	checkColum(columns,DBHelper.DICTIONARY_WORD);
				columns=	checkColum(columns,DBHelper.DICTIONARY_TRANSLATION);

				if (TextUtils.isEmpty(selection)) {
					selection = DBHelper.DICTIONARY_ID + " = " + id;
				} else {
					selection = selection + " AND " +DBHelper.DICTIONARY_ID  + " = " + id;
				}
				break;
			default:
				throw new IllegalArgumentException("Wrong URI: " + uri);
		}

		db = dbHelper.getWritableDatabase();
		Cursor cursor = db.query(DBHelper.TABL_DICTIONARY, columns, selection,
				selectionArgs, null, null, sortOrder);
		// просим ContentResolver уведомлять этот курсор
//		cursor.setNotificationUri(getContext().getContentResolver(),CONTENT_URI);
		return cursor;
	}

    private String checkSelection(String[] selectionArgs,String selection) {

         if(selectionArgs!=null) {
                if(TextUtils.isEmpty(selection))
                    return  DBHelper.DICTIONARY_WORD_forLike+" LIKE  ? ";
             else   return " AND " + DBHelper.DICTIONARY_WORD_forLike+" LIKE  ? ";
         };
        return selection;
    }


    private String[] checkColum(String[] columns, String word)
	{
		boolean tag = true;
		if (columns != null)
		{
			for (String column : columns)
				if (TextUtils.equals(column,word))
					tag = false;
			if (tag == true) {
			String[] newColumns = new String [ columns.length + 1];
			System.arraycopy(columns, 0, newColumns, 0, columns.length);
			newColumns[newColumns.length-1] = word;
				columns=newColumns;

			}
		}
		else columns=new String[]{word };
		return columns;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		if (uriMatcher.match(uri) !=URI_DICTIONARY)
			throw new IllegalArgumentException("Wrong URI: " + uri);

		db = dbHelper.getWritableDatabase();
		long rowID = db.insert(DBHelper.TABL_DICTIONARY, null, values);
		Uri resultUri = ContentUris.withAppendedId(uri, rowID);
		// уведомляем ContentResolver, что данные по адресу resultUri изменились
		getContext().getContentResolver().notifyChange(resultUri, null);
		return resultUri;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		return 0;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		return 0;
	}

	public String getType(Uri uri) {
//        switch (uriMatcher.match(uri)) {
//            case URI_BASE.URI_CONTACTS:
//                return CONTACT_CONTENT_TYPE;
//            case URI_CONTACTS_ID:
//                return CONTACT_CONTENT_ITEM_TYPE;
//        }
		return null;
	}

	@Override
	protected void finalize() throws Throwable {
		db.close();
		super.finalize();
	}
}
