package vitaliy.dictionary.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Vitaliy on 24.01.2015.
 */
public class DBHelper extends SQLiteOpenHelper {

    private static final String TAG ="DB_Helper" ;
    public static String DB_NAME ="Dictinary_BD";
    public static int DB_VERSION =1;

    public static String TABL_DICTIONARY ="Dictionary";

    public static String DICTIONARY_ID ="_id";
    public static String DICTIONARY_WORD ="Dictionary_WORD";
    public static String DICTIONARY_WORD_forLike ="Dictionary_WORD_for_like";
    public static String DICTIONARY_TRANSLATION ="Dictionary_TRANSLATION";


    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createDictionry(db);
    }

    private void createDictionry(SQLiteDatabase db) {
        Log.d(TAG, "--create table InstitutionsList");

        String Sql = "create table " + TABL_DICTIONARY + " " +
                "("
                + DICTIONARY_ID + " integer AUTO_INCREMENT  primary key, "
                + DICTIONARY_WORD + " text, "
                + DICTIONARY_WORD_forLike + " text, "
                + DICTIONARY_TRANSLATION + " text "
                + ");";
        db.execSQL(Sql);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
