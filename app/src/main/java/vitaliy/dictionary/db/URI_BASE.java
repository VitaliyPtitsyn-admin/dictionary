package vitaliy.dictionary.db;

import android.net.Uri;

/**
 * Created by Vitaliy on 24.01.2015.
 */
public interface URI_BASE
{
    public  static String AUTHORITY ="TestWorkDictionary";
    public  static String DICTIONARY="DictionaryTranslation";
    public  static String WORD=DICTIONARY+"/word";
    public  static String WORD_TRANSLATION=WORD+"/#";


	public static int URI_WORD =1;
	public static int URI_DICTIONARY=2;
	public static int URI_WORD_TRANSLATION=3;


    // Общий Uri
    public static final String CONTENT_URI = "content://"
            + AUTHORITY + "/";


    // набор строк
    static final String CONTACT_CONTENT_TYPE = "vnd.android.cursor.dir/vnd."
            + AUTHORITY  + "." + DICTIONARY;

    // одна строка
    static final String CONTACT_CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd."
            + AUTHORITY  + "." + DICTIONARY;


}
